#!/bin/sh
# Generación de llaves
KEYDIR=keys
mkdir -p $KEYDIR
ssh-keygen -f $KEYDIR/mealie < /dev/null

# Despliegue de AWS
terraform plan -out=tfplan                                      \
	-target=aws_instance.mealie_server                          \
	-target=aws_security_group.mealie                           \
	-target=aws_vpc_security_group_ingress_rule.allow_ssh_ipv4  \
	-target=aws_vpc_security_group_ingress_rule.allow_http_ipv4 \
	-target=aws_vpc_security_group_egress_rule.allow_all_ipv4   \
	-var "pk_spec=$(cat keys/mealie.pub)" &&
terraform apply -auto-approve tfplan || exit 1

instance_ip=$(terraform output instance_public_ip)
instance_ip=${instance_ip#\"}
instance_ip=${instance_ip%\"}

until ssh -o StrictHostKeyChecking=no -i keys/mealie _docker@$instance_ip echo "Servidor ssh listo"; do
	echo "Esperando un poco a que el acceso ssh esté listo"
	sleep 3
done
# Despliegue de toda la infraestructura
terraform apply -auto-approve -var "pk_spec=$(cat keys/mealie.pub)"
rm tfplan