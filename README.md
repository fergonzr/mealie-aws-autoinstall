# Auto-installador Mealie en AWS

Este repositorio contiene todo lo necesario para desplegar automáticamente una instancia de [Mealie](https://docs.mealie.io/), una aplicación web de administración de recetas, en la nube AWS.
El instalador, `setup.sh`, crea una instancia de EC2 t2.micro que corre Debian 12 en la que posteriormente se le instala docker y, sobre este, Mealie.
La infraestructura y configuración de despliegue puede ser manejada con terraform haciendo los cambios correspondientes a los archivos `*.tf`, pero utilizando este mismo script en vez de simplemente `terraform apply`.

## Instalación

0. Prerrequisitos:
* Una cuenta en AWS
* AWS CLI (está en los repositorios de muchas distribuciones, `aws-cli` en Void y Arch, `awscli` en Debian,...)
* Terraform
* POSIX sh

1. En primer lugar, se debe asegurar que AWS CLI puede conectarse y autenticarse correctamente a nuestra cuenta de AWS.
Normalmente, esto implica crear un nuevo usuario en el servicio de IAM de AWS.
Si eres estudiante, sin embargo, puedes obtener los credenciales necesarios en la ventana de lanzamiento del Learner lab, haciendo click en `AWS Details` > `AWS CLI`.
Copia y pega el contenido de la caja de texto que se abre en `~/.aws/credentials`.
Luego ejecuta el comando `aws configure`.
Puedes dejar en blanco el Key ID y el Access Key, con haberlos puesto en `~/.aws/credentials` es suficiente, lo importante es especificar la región.
En el learner lab, encontrarás esta en el fondo del menú lateral.

2. Clona el repositorio e ingresa en él:

```
$ git clone https://gitlab.com/fergonzr/mealie-aws-autoinstall.git
$ cd mealie-aws-autoinstall
```

3. Ejecuta `setup.sh`:

```
$ ./setup.sh
```

4. Disfruta de todo el texto que verás durante los siguientes minutos. No se requiere ninguna información adicional. No obstante, es importante que no tengas nada de lo siguiente en AWS:
- Una instancia de EC2 llamada `mealie_server`.
- Un grupo de seguridad llamado `mealie`.
- Un par de llaves llamado `mealie`.

Si es el caso, elimínalos, ya que puede ocasionar conflictos en la instalación.
Para eliminar limpiamente toda la infraestructura levantada por este script, lee la sección *Eliminación*.

5. Al final de la instalación, terraform mostrará la ip pública de la instancia. Luego de esperar unos segundos para que Mealie aranque, copia y pégala en un navegador web.
6. Deléitate con tu nueva instalación de Mealie.

## Administración

El instalador genera un par de llaves para la instancia en el directorio `keys/`, el cual puede ser utilizado para acceso remoto a la instancia si es necesario.
Es posible hacer cualquier cambio en la configuración de despliegue de mealie o la infraestructura de AWS modificando el contenido de `mealie.tf` o `aws.tf`, respectivamente.
Para aplicar estos cambios, se recomienda utilizar de nuevo el script `setup.sh`, ya que este se encargará de asegurarse de que haya una conexión SSH exitosa a la instancia antes de aplicar toda la configuración relacionada con docker.

## Eliminación

En el repositorio también encontrarás el script `delete.sh`, el cual elimina toda esta infraestructura usando `terraform destroy`, nuevamente asegurándose de hacerlo en el orden adecuado:

```
# Eliminar todo
$ ./delete.sh
```

Hacerlo con `terraform destroy` directamente **puede llevar a resultados inconsistentes**.
A veces terraform intenta eliminar la imagen de Mealie antes de que su servicio esté completamente eliminado, lo cual no es posible dado que depende de esta.
Si esto ocurre, sólo ejecuta el script de nuevo.
Es de notar que `delete.sh` no elimina el par de llaves creado, por lo que tendrás que eliminar el directorio `keys/` manualmente si quieres re-generarlas.

```
$ rm -r keys/
```

## Fuentes
- [Terraform: Proveedor docker](https://registry.terraform.io/providers/kreuzwerker/docker/latest/docs)
- [Terraform: Proveedor AWS](https://registry.terraform.io/providers/hashicorp/aws/latest/docs)
- [Página principal de Mealie](https://docs.mealie.io/)
