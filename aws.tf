provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "mealie_server" {
  ami             = "ami-058bd2d568351da34"
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.mealie.name]

  key_name = aws_key_pair.mealie.key_name

  # instalar docker, y añadir el usuario _docker al sistema.
  # habilitar acceso SSH al mismo a través de la misma llave que admin
  user_data = <<-EOF
#!/bin/bash
sudo apt-get update -y
sudo apt-get install -y docker docker-compose
mkdir -p /var/_docker/.ssh/
sudo useradd -d /var/_docker/ -G docker _docker
sudo install -o _docker /home/admin/.ssh/authorized_keys /var/_docker/.ssh/authorized_keys
sudo docker swarm init
EOF


  tags = {
    Name = "Mealie"
  }
}

# Grupos y reglas de seguridad

resource "aws_security_group" "mealie" {
  name        = "mealie"
  description = "Allow SSH and HTTP inbound traffic"
}

resource "aws_vpc_security_group_ingress_rule" "allow_ssh_ipv4" {
  security_group_id = aws_security_group.mealie.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 22
  ip_protocol       = "tcp"
  to_port           = 22
}

resource "aws_vpc_security_group_ingress_rule" "allow_http_ipv4" {
  security_group_id = aws_security_group.mealie.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 80
  ip_protocol       = "tcp"
  to_port           = 80
}

resource "aws_vpc_security_group_egress_rule" "allow_all_ipv4" {
  security_group_id = aws_security_group.mealie.id
  cidr_ipv4         = "0.0.0.0/0"
  ip_protocol       = "-1"
}

# Par de llaves. Se generan con setup.sh

resource "aws_key_pair" "mealie" {
  key_name   = "mealie"
  public_key = var.pk_spec
}
