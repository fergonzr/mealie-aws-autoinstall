provider "docker" {
  # Conexión ssh hacia la instancia
  host     = format("ssh://_docker@%s:22", aws_instance.mealie_server.public_ip)
  ssh_opts = ["-i", "keys/mealie", "-o", "StrictHostKeyChecking=no"]
}

resource "docker_image" "mealie" {
  depends_on = [aws_instance.mealie_server]
  name         = "ghcr.io/mealie-recipes/mealie:v1.7.0"
  keep_locally = false
}

resource "docker_service" "mealie" {
  depends_on = [aws_instance.mealie_server]
  name       = "mealie-service"

  task_spec {
    container_spec {
      image = docker_image.mealie.repo_digest

      mounts {
        target = "/app/data"
        source = docker_volume.mealie_volume.name
        type   = "volume"
      }
      env = {
        ALLOW_SIGNUP = "true"
        TZ           = "America/Bogota"
        BASE_URL     = format("http://%s", aws_instance.mealie_server.public_ip)
      }
    }
    resources {
      limits {
        memory_bytes = 1000000000
      }
    }
  }
  # Exponer el servidor en el puerto 80
  endpoint_spec {
    ports {
      target_port    = "9000"
      published_port = "80"
    }
  }
}

resource "docker_volume" "mealie_volume" {
  depends_on = [aws_instance.mealie_server]
  name = "mealie-volume"
}
