terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 3.0.2"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }
  required_version = ">= 1.2.0"
}

variable "pk_spec" {
  description = "Especificación de la llave ssh pública de la instancia de Mealie"
  type        = string
}

output "instance_public_ip" {
	description = "IP pública de la instancia de Mealie"
	value = aws_instance.mealie_server.public_ip
}
