#!/bin/sh
# Eliminar primero todo lo relacionado con docker
terraform plan -var "pk_spec=$(cat keys/mealie)" -destroy -out=tfplan          \
	-target=docker_image.mealie                                 \
	-target=docker_service.mealie                           \
	-target=docker_volume.mealie_volume &&
terraform apply tfplan &&
terraform destroy -var "pk_spec=$(cat keys/mealie)"
rm tfplan
